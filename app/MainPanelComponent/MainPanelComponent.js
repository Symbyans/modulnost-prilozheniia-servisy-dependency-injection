'use strict';


angular
.module('myApp')
.component('mainPanelComponent', {
	templateUrl: 'MainPanelComponent/MainPanelComponent.html',
})
.controller('MainPanelCtrl', ['$scope', function($scope) {
        $scope.items = [{
            "name"  : "Список",
            "url"   : "list"
        },{
            "name"  : "Добавить нового",
            "url"   : "createNewPokemon"
        },{
            "name"  : "Личный кабинет",
            "url"   : "myaccount"
        }];


}]);
