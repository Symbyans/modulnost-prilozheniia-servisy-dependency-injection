'use strict';

angular
.module('myApp')
.controller('AccountComponentCtrl', function($state, $stateParams, AccountLogin) {
    var vm = this;
    vm.account = false;

    vm.addUser = function(item){
        vm.accountForm.$setPristine();
        AccountLogin.addUserData(item);
        vm.getUser = AccountLogin.getUserData();
        vm.account = true;
    };
    vm.removeUser = function() {
        vm.newUser = {};        
        AccountLogin.removeUserData();
        vm.getUser = AccountLogin.getUserData();
        vm.account = false;
     };
})
