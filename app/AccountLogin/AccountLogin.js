'use strict';

angular
    .module('myApp')
    .factory('AccountLogin', function() {
        const state = {
            userData: {}
        };

        return {
            getUserData()  {
                return state.userData;
            },
            addUserData(item) {
             state.userData = {
                "FirstName" : item.firstName,
                "LastName" : item.lastName,
                "Email" : item.email,
                "Phone" : item.phone
            }},
            removeUserData() {
               state.userData = {};
            }
        };

    })
